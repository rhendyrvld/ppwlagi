from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from datetime import datetime
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_index_url_exist(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_index_use_index_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_content(self):
        response = self.client.get('')
        contents = response.content.decode('utf8')
        self.assertIn('Halo, apa kabar?', contents)

    def test_model_can_create_new_status(self):
        now = datetime.now()
        status = Status.objects.create(message='Coba duls', time=now)

        count_statuses = Status.objects.all().count()
        self.assertEqual(count_statuses, 1)
    
    def test_model_returns_message(self):
        now = datetime.now()
        status = Status.objects.create(message='Coba duls', time=now)

        self.assertEqual(str(status), 'Coba duls')

    def test_status_posted(self):
        response = self.client.post('', {'message' : "Try",})
        self.assertIn("Try", response.content.decode())

class Lab6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)
        
        message = selenium.find_element_by_id('id_message')
        submit = selenium.find_element_by_id('submit_button')

        message.send_keys('Coba Coba')
        time.sleep(5)

        submit.send_keys(Keys.RETURN)
        time.sleep(5)

        selenium.get('http://127.0.0.1:8000/')
        assert 'Coba Coba' in selenium.page_source