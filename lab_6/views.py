from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .models import Status
from .forms import StatusForm

# Create your views here.

def index(request):
    form = StatusForm(request.POST or None)
    response = {'form' : form}
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        add = Status(message=response['message'])
        add.save()
        lists = Status.objects.all().values()
        response['lists'] = lists
        return render(request, 'index.html', response)
    else:
        lists = Status.objects.all().values()
        response['lists'] = lists
        return render(request, 'index.html', response)