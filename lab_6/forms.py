from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    class Meta:
        model = Status

        fields = ['message']

        widgets = {
            'message' : forms.Textarea(attrs={'class' : 'form-control'})
        }