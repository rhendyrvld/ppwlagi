from django.db import models
from django.utils import timezone
from datetime import *

# Create your models here.

class Status(models.Model):
    message = models.TextField(max_length = 300)
    time = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.message